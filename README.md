This script will take in 3 mandatory parameters along with 1 optional paramater:

source folder, destination folder, file extension, and the optional # of seconds between sub-folder processing.

With this it will scan all sub folders of the source folder for all files with the passed extension and send them all to the destination folder.