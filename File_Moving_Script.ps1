﻿<#
    Written By: Christo Mavrick
    Date:       February 6th, 2017
    Version:    2.0
	
    Description: This script will take in 3 mandatory parameters along with 1
    optional paramater: source folder, destination folder, file extension, and
    the optional # of seconds between sub-folder processing. With this it will
    scan all sub folders of the source folder for all files with the passed
    extension and send them all to the destination folder.
#>
#Sets strictmode to avoid lots of issues that can popup in PS scripts
set-strictmode -version 2.0

[bool]$firstError = $True
#Grab the path to the script and split on everything after the final \ to get the file name
$thisScriptName = Split-Path $MyInvocation.MyCommand.Definition -Leaf
#And now remove the extension for further use
$thisScriptNameNoExt = [IO.Path]::GetFileNameWithoutExtension($thisScriptName)

#Functions
function Retry-Command
{
    param (
    [Parameter(Mandatory=$true)][string]$command, 
    [Parameter(Mandatory=$true)][hashtable]$args, 
    [Parameter(Mandatory=$false)][int]$retries = 3, 
    [Parameter(Mandatory=$false)][int]$secondsDelay = 2
    )

    #This function implemented by/at https://blogs.endjin.com/2014/07/how-to-retry-commands-in-powershell/
    #Modified by Christo Mavrick
    
    # Setting ErrorAction to Stop is important. This ensures any errors that occur in the command are 
    # treated as terminating errors, and will be caught by the catch block.
    $args.ErrorAction = "Stop"
    
    $retrycount = 0
    $completed = $false

    while (-not $completed) {
        try {
            & $command @args
            Write-Verbose ("Command [{0}] succeeded." -f $command)
            $completed = $true
        } catch {
            $currentDateTime = Get-Date -Format g
            
            if ($firstError) {
                New-Item $PWD\$thisScriptNameNoExt.txt -type file -Force
                $firstError = $False
            }
            
            if ($retrycount -ge $retries) {
                Write-Output ("$currentDateTime -- Command [{0}] failed the maximum number of {1} times." -f $command, $retrycount) >> "$thisScriptNameNoExt.txt"
                throw
            } else {
                Write-Output ("$currentDateTime -- Command [{0}] failed. Retrying in {1} seconds." -f $command, $secondsDelay) >> "$thisScriptNameNoExt.txt"
                Start-Sleep $secondsDelay
                $retrycount++
            }
        }
    }
}

#Capture arguments passed on the command line
$actionType = $args[0]
$sourceFolder = $args[1]
$destinationFolder = $args[2]
$fileExtension = $args[3]
if ($args.Length -eq 5) { #Since the 5th parameter is optional only try to set it from the arg array if the user actually passed it
    if ($args[4] -match "^\d+$") { #Regex for any positive numeric value
        $timedGap = $args[4]
    }
    else {
        Write-Host "Seconds must be a numeric value!"-ForegroundColor Red -BackgroundColor Black
        Exit 1
    }
}
else {
    $timedGap = 0 #Time in seconds, no delay
}

#Parameter Validation
if ($args.Length -lt 4 -or $args.Length -gt 5) { #if there were not 4 or 5 arguments passed
    Write-Host "Usage: .\$thisScriptName COPY/MOVE SOURCE_FOLDER DESTINATION_FOLDER FILE_EXTENSION (optional)SECONDS_BETWEEN_SUB_FOLDER_PROCESSING"-ForegroundColor Red -BackgroundColor Black
    Exit 1
}

$isCopyOrMove = $actionType -match "\bcopy\b|\bmove\b" #\b is a boundary, using here to make sure we allow strict/exact match
if ($isCopyOrMove -ne $True) {
    Write-Host "The first parameter can only be 'Copy' or 'Move'!"-ForegroundColor Red -BackgroundColor Black
    Exit 1
}
else { #The first param was copy or move, figure out which one
    $isCopy = $actionType -match "\bcopy\b"
    $isMove = $actionType -match "\bmove\b"
    #Maybe this can be simplified and just checked above? Idea for future optimization...
}

$sourceTest = Retry-Command -command 'Test-Path' -args @{ Path = $sourceFolder }
$destTest = Retry-Command -command 'Test-Path' -args @{ Path = $destinationFolder }
if ($sourceTest -ne $True -or $destTest -ne $True) { #if either the source or destination folders do not exist
    Write-Host "Make sure that both the source and destination folders are valid paths and exist!"-ForegroundColor Red -BackgroundColor Black
    Exit 1
}

if (($fileExtension -match "^\..{2}.*") -ne $True) { #if the file ext is in format ".cc*" where c = a character
    Write-Host "File extension must be a valid format! (E.g: .uhh)"-ForegroundColor Red -BackgroundColor Black
    Exit 1
}

#Main Block

#If file !MOVE! fails, retry 3 times then quietly skip, unless it fails for already existing then just log it + force move
#Log all move/file related issues to a file, only if they happen of course
#First error of each run overrides, subsequent ones append

#The next line recursively grabs all the files under the source folder that have the specified passed file
#extension and puts them into a <List> variable, the FullName is also grabbed instead to have the absolute path
$fileList = Get-ChildItem $sourceFolder -Recurse | where {$_.extension -eq $fileExtension} | % { $_.FullName }
#Might need to add/figure out how to use Retry-Command for the above piped line?
#Shouldn't need to though, any permission error should be thrown above during the validation check

foreach ($file in $fileList) {

    if ($isMove) {
        #Move-Item with Retry-Command function to retry 3 times and log in case of error
        Retry-Command -command 'Move-Item' -args @{ Path = $file; Destination = $destinationFolder; Force = $True }
    }
    if ($isCopy) {
        Retry-Command -command 'Copy-Item' -args @{ Path = $file; Destination = $destinationFolder; Force = $True }
    }
    #If above line(s)/logic causes problems for some reason, just use the confirmed working but lack or retry/error-checking:
    #Move-Item $file $destinationFolder -force

    Start-Sleep $timedGap #For optional gap switch, 0 by default which means no gap at all
}

Exit 0